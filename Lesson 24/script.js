'use strict';

// window.addEventListener('scroll', (event) => {
//     console.log(event);
// });

/* Исчезающий хеадер
let previousScrollY = 0;
//обработчик события - скролл мыши
window.addEventListener('scroll', (event) => {
    console.log(window.scrollY);
    const header = document.getElementById('header');

    if (window.scrollY > previousScrollY && window.scrollY > 50) {
        header.style.display = 'none';
    } else {
        if (window.scrollY < previousScrollY) {
            header.style.display = '';
        }
    }
    previousScrollY = window.scrollY;
});
*/


/*
//Получаем элемент с классом square-1
const square1 = document.querySelector('.square-1');
const square2 = document.querySelector('.square-2');
const square3 = document.querySelector('.square-3');
//создаём обработчик события - нажатие мыши

// погружение
square1.addEventListener('click', () => {
    console.log('square-1 click погружение');
}, true);
square2.addEventListener('click', () => {
    console.log('square-2 click погружение');
}, true);
square3.addEventListener('click', () => {
    console.log('square-3 click погружение');
}, true);

document.addEventListener('click', () => {
    console.log('click anywhere погружение');
}, true);

// всплытие
square1.addEventListener('click', () => {
    console.log('square-1 click всплытие');
});
square2.addEventListener('click', () => {
    console.log('square-2 click всплытие');
});
square3.addEventListener('click', () => {
    console.log('square-3 click всплытие');
});

*/
document.addEventListener('click', () => {
    console.log('click anywhere всплытие');
});


// выбираем первый селектор с классом button
const button = document.querySelector('.button');
button.addEventListener('click', (event) => {
    // console.log(event);
    console.log('Кнопка клик 1');
    // отменяет стандартное поведение кнопки
    event.preventDefault();

    //event.stopPropagation();// предотвращает всплытие
    //event.stopImmediatePropagation();//на всплатие
});

button.addEventListener('click', (event) => {
    event.preventDefault();
    console.log('Кнопка клик 2');
    event.stopImmediatePropagation();//на погружение
}, true);


/*
const input = document.querySelector('[name=input]');
console.log({input});
*/

/*
// на input навешиваем событие и выводим его в консоль
const input = document.querySelector('[name=input]');
input.addEventListener('input', (event) => {
   console.log(event.target.value);
   //если ввести red, то граница инпута будет красная
    if(event.target.value === 'red') {
        input.style.border = '1px solid red';
        event.target.value = 'green';//меняет значение на текст green
    }
});
*/

//
const input = document.querySelector('[name=input]');
input.addEventListener('input', inputHandler);
input.addEventListener('input', () => console.log('Анонимная функция'));

//делает тоже само что и на строке 92, только работает на всплатие
// input.oninput = () => {console.log('oninput');}

// input.removeEventListener('input', inputHandler);//удаляет обработчик
input.removeEventListener('input', () => console.log('Анонимная функция'));//не должно так сработать

function inputHandler(event) {
    console.log(event.target.value);
    //если ввести red, то граница инпута будет красная
    if(event.target.value === 'red') {
        input.style.border = '1px solid red';
        event.target.value = 'green';//меняет значение на текст green
    }
}

//вывод событий на движение мышкой
// window.addEventListener('mousemove', (event) => {
//     console.log(event);
// });

//вывод значения по осям на движение мышкой с учётом скрола
// window.addEventListener('mousemove', (event) => {
//     console.log(event.clientX, event.clientY);
// });

//вывод значения по осям на движение мышкой не зависиьт от положения скролла
// window.addEventListener('mousemove', (event) => {
//     console.log(event.offsetX, event.offsetY);
// });

//на нажатие кнопки мыши вниз
window.addEventListener('mousedown', (event) => {
    console.log(event.offsetX, event.offsetY);
});

//на отжатие кнопки мыши вверх
window.addEventListener('mouseup', (event) => {
    console.log(event.offsetX, event.offsetY);
});