'use strict';

//Основное задание.
function makeFibonacciFunction() {
    let a = 0;
    let b = 1;
    return function () {
        let f = a + b;
        a = b;
        b = f;
        return a;
    }
}

const fibonacci = makeFibonacciFunction();

console.log(fibonacci()); // Вывод в консоль: 1
console.log(fibonacci()); // Вывод в консоль: 1
console.log(fibonacci()); // Вывод в консоль: 2
console.log(fibonacci()); // Вывод в консоль: 3
console.log(fibonacci()); // Вывод в консоль: 5
console.log(fibonacci()); // Вывод в консоль: 13
console.log(fibonacci()); // Вывод в консоль: 21
console.log(fibonacci()); // Вывод в консоль: 34


/*
// Задание со звёздочкой

const fibonacci = (function () {
    let a = 0;
    let b = 1;
    return  function () {
        let f = a + b;
        a = b;
        b = f;
        console.log(a);
    }
})();

fibonacci(); // Вывод в консоль: 1
fibonacci(); // Вывод в консоль: 1
fibonacci(); // Вывод в консоль: 2
fibonacci(); // Вывод в консоль: 3
fibonacci(); // Вывод в консоль: 5
fibonacci(); // Вывод в консоль: 8
fibonacci(); // Вывод в консоль: 13
fibonacci(); // Вывод в консоль: 21
fibonacci(); // Вывод в консоль: 34
fibonacci(); // Вывод в консоль: 55

*/