import './MyComponent.css';
import PropTypes from "prop-types";
// import MyClassComponent from "../MyClassComponent/MyClassComponent";

/* применение props
function MyComponent(props) {
    console.log(props)
    return (
        <div className="MyComponent">
            Привет, {props.name}!
            {/!*<MyClassComponent/>*!/}
        </div>
    );
}
*/

// Деструктуризация
function MyComponent({name = 'Ddefault prop name', lastName = 'Default prop lastname'}) {

    return (
        <div className="MyComponent">
            Привет, {name} {lastName}!
            {/*<MyClassComponent/>*/}
        </div>
    );
}

MyComponent.propTypes = {
    name: PropTypes.string,
    lastName: PropTypes.string
}

export default MyComponent;
