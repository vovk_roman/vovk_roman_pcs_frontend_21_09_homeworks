import React from 'react';
import PropTypes from 'prop-types';


/* вывод props
export default class MyClassComponent extends React.Component {
    render() {
        console.log(this.props) //так можно достучаться до props
        return (
            <div>
                Привет Class, {this.props.name}!
            </div>
        )
    }
}*/

// Деструктуризация
export default class MyClassComponent extends React.Component {
    //задаём дефолтные значения
    static propTypes = {
        name: PropTypes.string,
        lastName: PropTypes.string
    }
    static defaultProps = {
        name: 'Default Last Name',
        lastName: 'Default Last Name'
    }
    render() {
        const {name, lastName, children} = this.props;
        console.log(name) //так можно достучаться до props
        return (
            <div>
                Привет Class, {name} {lastName}!
                <div>
                    Здесь будет строчка, которая вставлена внутрь тега компонента:<br/>
                    {children}
                </div>
            </div>
        )
    }
}