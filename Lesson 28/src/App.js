import './App.css';
import MyComponent from './components/MyComponent/MyComponent';
import MyClassComponent from "./components/MyClassComponent/MyClassComponent";
import Card from "./components/Card/Card";

function App() {
    const firstName = 'Roman'
    return (
        <div className="App">
            {/*<MyComponent name={'Роман'}/>*/}  {/*если нужно вставить props*/}
            {/*<MyComponent name={firstName}/>*/}  {/*если нужно вставить переменную*/}
            <MyComponent name={"Какой-то текст"}/>  {/*если нужно вставить текст*/}
            <MyComponent /> {/*сюда будет передан default prop name*/}
            <MyComponent lastName={'Гулин'}/> {/*сюда будет передан default prop lastname*/}
            <MyComponent name={'Дмитрий'}/>
            <MyComponent name={'Алексей'}/>
            <MyComponent name={'Кирилл'}/>
            <MyComponent name={'Олег'}/>
            <MyComponent name={'Матвей'}/>

            {/*Через классы вывести props*/}
            <MyClassComponent name={'Игорь'} lastName={'Петухов'}/>
            <MyClassComponent>
                <div>Привет, Борис!!!!</div>
            </MyClassComponent>
            <MyClassComponent />
            <Card/>
            <Card/>
            <Card/>
            <Card/>
            <Card/>
        </div>
    );
}

export default App;
