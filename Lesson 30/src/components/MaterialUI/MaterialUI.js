import Button from '@mui/material/Button';
import {TextField} from "@mui/material";

function MaterialUI() {
    return (
        <div>
            <Button variant="contained">Hello MaterialUI</Button>
            <TextField id="outlined-basic" label="Outlined" variant="outlined" />
            <TextField id="filled-basic" label="Заполненный" variant="filled" />
            <TextField id="standard-basic" label="Стандартный" variant="standard" />
        </div>
    )
}

export default MaterialUI;