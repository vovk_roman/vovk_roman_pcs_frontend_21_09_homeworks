'use strict';

/*
// sessionStorage
let inputValue = sessionStorage.getItem('inputValue') || '';
console.log('Изначальное значение inputValue', inputValue);
const input = document.getElementById('input');
input.value = inputValue; // помещает в поле input значение inputValue

input.addEventListener('input', (event) => {
    inputValue = event.target.value;
    sessionStorage.setItem('inputValue', event.target.value);
    console.log(inputValue);
});
*/


/*
// localStorage
let inputValue = localStorage.getItem('inputValue') || '';
console.log('Изначальное значение inputValue', inputValue);
const input = document.getElementById('input');
input.value = inputValue; // помещает в поле input значение inputValue

input.addEventListener('input', (event) => {
    inputValue = event.target.value;
    localStorage.setItem('inputValue', event.target.value);
    console.log(inputValue);
});
*/

/*

// cookies
let inputValue = localStorage.getItem('inputValue') || '';
console.log('Изначальное значение inputValue', inputValue);
const input = document.getElementById('input');
input.value = inputValue; // помещает в поле input значение inputValue
document.cookie = 'name' + '=' + 'Roman Vovk';

input.addEventListener('input', (event) => {
    inputValue = event.target.value;
    document.cookie = 'inputValue' + '=' + event.target.value;
    console.log(inputValue);
});
*/


// cookies через библиотеку
let inputValue = localStorage.getItem('inputValue') || '';
console.log('Изначальное значение inputValue', inputValue);
const input = document.getElementById('input');
input.value = inputValue; // помещает в поле input значение inputValue
Cookies.set('name', 'Roman Vovk');
console.log(Cookies.get('name')); //так получаем значение кука, указав ключ и выводим в консоль

input.addEventListener('input', (event) => {
    inputValue = event.target.value;
    document.cookie = 'inputValue' + '=' + event.target.value;
    console.log(inputValue);
});

















