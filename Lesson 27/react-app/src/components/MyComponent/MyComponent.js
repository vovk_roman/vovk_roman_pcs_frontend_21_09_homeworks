import './MyComponent.css';
import MyClassComponent from "../MyClassComponent/MyClassComponent";

function MyComponent() {
    return (
        <div className="MyComponent">
            Привет, MyComponent!
            <MyClassComponent/>
        </div>
    );
}

export default MyComponent;
