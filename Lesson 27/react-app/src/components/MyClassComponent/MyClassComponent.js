import React from 'react';

export default class MyClassComponent extends React.Component {
    render() {
        return (
            <div>
                Привет Class Component!
            </div>
        )
    }
}