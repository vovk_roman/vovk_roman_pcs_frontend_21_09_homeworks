'use strict';

if (confirm('Хотите посчитать на упрощённом калькуляторе?')) {
    const number1 = prompt('Введите первое число');
    const sign = prompt('Введите один из следующих знаков (+, -, /, *)');
    const number2 = prompt('Введите второе число');
    let result;

    if (number1 === '') {
        alert('Первое число не указаано');
    } else if (isNaN(number1)) {
        alert('Введено не число');
    } else if (!sign) {
        alert('Не введён знак');
    } else if (sign !== '+' && sign !== '-' && sign !== '/' && sign !== '*') {
        alert('Программа не поддерживает такую операцию');
    } else if (number2 === '') {
        alert('Второе число не указаано');
    } else if (isNaN(number2)) {
        alert('Введено не число');
    } else if (sign === '+') {
        alert(`Результат вычислений : ${result = +number1 + +number2}`);
    } else if (sign === '-') {
        alert(`Результат вычислений : ${result = +number1 - +number2}`);
    } else if (sign === '/' && +number2 === 0) {
        alert(`Результат вычислений : ${result = +number1 / +number2}, но в математике делить на НОЛЬ нельзя`);
    } else if (sign === '/') {
        alert(`Результат вычислений : ${result = +number1 / +number2}`);
    } else if (sign === '*') {
        alert(`Результат вычислений : ${result = +number1 * +number2}`);
    }
} else alert('Ну как хотите! А я так старался(');