import React, {useEffect} from 'react'
import {routerService} from '../../services/routerService'
import {getAuth} from "../../services/authStorageService";
import {observer} from "mobx-react-lite";
import AuthState from "../../store/AuthState";

export const App = observer(() => {
  const isAuth = AuthState.isAuth

  useEffect(() => {
    const auth = getAuth()
    if (auth === 'true') {
      return AuthState.loginHandler()
    }
  }, [])

  const router = routerService(isAuth)

  return (
    <div>
      {router}
    </div>
  )
})
