import React from "react";
import styles from './Footer.module.css'
import {FooterMenu} from './components/FooterMenu/FooterMenu'

export const Footer = () => {
  return (
    <div className={styles.wrapper}>
      <FooterMenu/>
    </div>
  )
}