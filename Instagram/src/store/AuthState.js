import {makeAutoObservable} from "mobx";

class AuthState {
  constructor() {
    makeAutoObservable(this)
  }

  isAuth = false

  loginHandler = () => {
    this.isAuth = true
  }

  logoutHandler = () => {
    this.isAuth = false
  }

}

export default new AuthState()