const lsName = 'inst_auth'


export const setAuth = () => {
    localStorage.setItem(lsName, 'true')
}

export const getAuth = () => {
    return localStorage.getItem(lsName)
}

export const deleteAuth = () => {
    return localStorage.removeItem(lsName)
}

const regName = 'reg_inst'

export const getRegistration = () => {
    return localStorage.getItem(regName)
}

export const setRegistration = (login, password) => {
    const lsOld = JSON.parse(getRegistration())
    console.log(lsOld)
    const newArray = lsOld !== null ? [...lsOld, {login, password}] : [{login, password}]
    return localStorage.setItem(regName, JSON.stringify(newArray))
}