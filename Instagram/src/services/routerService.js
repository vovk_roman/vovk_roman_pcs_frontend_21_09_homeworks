import React from "react";
import {Routes, Route, Navigate} from 'react-router-dom'
import {MainPage} from "../pages/MainPage/MainPage";
import {UserPage} from '../pages/UserPage/UserPage'
import {UsersPage} from "../pages/UsersPage";
import PhotoPage1 from "../pages/PhotoPage/PhotoPage1";
import PhotoPage2 from "../pages/PhotoPage/PhotoPage2";
import PhotoPage3 from "../pages/PhotoPage/PhotoPage3";
import PhotoPage4 from "../pages/PhotoPage/PhotoPage4";
import PhotoPage5 from "../pages/PhotoPage/PhotoPage5";
import PhotoPage6 from "../pages/PhotoPage/PhotoPage6";

export const routerService = (isAuth) => {
    if (isAuth) {
        return (
            <Routes>
                <Route path={'/user'} element={<UserPage/>}/>
                <Route path={'/users'} element={<UsersPage/>}/>
                <Route path={'/photo1'} element={<PhotoPage1/>}/>
                <Route path={'/photo2'} element={<PhotoPage2/>}/>
                <Route path={'/photo3'} element={<PhotoPage3/>}/>
                <Route path={'/photo4'} element={<PhotoPage4/>}/>
                <Route path={'/photo5'} element={<PhotoPage5/>}/>
                <Route path={'/photo6'} element={<PhotoPage6/>}/>
                <Route path={'*'} element={<Navigate to={'/user'}/>}/>
            </Routes>
        )
    }

    return (
        <Routes>
            <Route path={'/'} element={<MainPage/>}/>
            <Route path={'/users'} element={<UsersPage/>}/>
            <Route path={'*'} element={<Navigate to={'/'}/>}/>
        </Routes>
    )
}
