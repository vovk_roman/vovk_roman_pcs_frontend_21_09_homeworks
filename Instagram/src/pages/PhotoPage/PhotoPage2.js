import React from 'react';
import {Layout} from "../../components/Layout/Layout";
import photo2 from '../../assets/images/photo2.jpeg'
import styles from "./PhotoPage.module.css";

const PhotoPage2 = () => {
    return (
        <Layout>
            <div className={styles.wrapper}>
                <img src={photo2} alt={''}/>
            </div>
        </Layout>
    );
};

export default PhotoPage2;