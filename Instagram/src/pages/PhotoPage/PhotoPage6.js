import React from 'react';
import {Layout} from "../../components/Layout/Layout";
import photo6 from '../../assets/images/photo6.jpeg'
import styles from "./PhotoPage.module.css";

const PhotoPage6 = () => {
    return (
        <Layout>
            <div className={styles.wrapper}>
                <img src={photo6} alt={''}/>
            </div>
        </Layout>
    );
};

export default PhotoPage6;