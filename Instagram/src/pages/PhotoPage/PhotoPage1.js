import React from 'react';
import styles from "./PhotoPage.module.css";
import {Layout} from "../../components/Layout/Layout";
import photo1 from '../../assets/images/photo1.jpeg'

const PhotoPage1 = () => {
    return (
        <Layout>
            <div className={styles.wrapper}>
                <img src={photo1} alt={''}/>
            </div>
        </Layout>
    );
};

export default PhotoPage1;