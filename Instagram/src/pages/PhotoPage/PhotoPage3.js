import React from 'react';
import {Layout} from "../../components/Layout/Layout";
import photo3 from '../../assets/images/photo3.jpeg'
import styles from "./PhotoPage.module.css";

const PhotoPage3 = () => {
    return (
        <Layout>
            <div className={styles.wrapper}>
                <img src={photo3} alt={''}/>
            </div>
        </Layout>
    );
};

export default PhotoPage3;