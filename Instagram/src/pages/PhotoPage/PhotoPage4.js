import React from 'react';
import {Layout} from "../../components/Layout/Layout";
import photo4 from '../../assets/images/photo4.jpeg'
import styles from "./PhotoPage.module.css";

const PhotoPage4 = () => {
    return (
        <Layout>
            <div className={styles.wrapper}>
                <img src={photo4} alt={''}/>
            </div>
        </Layout>
    );
};

export default PhotoPage4;