import React from 'react';
import {Layout} from "../../components/Layout/Layout";
import photo5 from '../../assets/images/photo5.jpeg'
import styles from "./PhotoPage.module.css";

const PhotoPage5 = () => {
    return (
        <Layout>
            <div className={styles.wrapper}>
                <img src={photo5} alt={''}/>
            </div>
        </Layout>
    );
};

export default PhotoPage5;