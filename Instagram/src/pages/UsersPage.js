import React from "react";
import styles from './UsersPage.module.css'
import photo1 from '../assets/images/photo1.jpeg'
import photo2 from '../assets/images/photo2.jpeg'
import photo3 from '../assets/images/photo3.jpeg'
import photo4 from '../assets/images/photo4.jpeg'
import photo5 from '../assets/images/photo5.jpeg'
import photo6 from '../assets/images/photo6.jpeg'
import {Link} from "react-router-dom";

const Item = ({name, img}) => {
    return (
        <div className={styles.photo}>
            {/*<p>Name: {name}</p>*/}
            <Link to={`/photo${name}`}>
                <img src={img} alt={''}/>
            </Link>
        </div>
    )
}

const UsersList = () => {
    const arr = [
        {
            id: 1,
            name: '1',
            img: photo1
        },
        {
            id: 2,
            name: '2',
            img: photo2
        },

        {
            id: 3,
            name: '3',
            img: photo3
        },

        {
            id: 4,
            name: '4',
            img: photo4
        },

        {
            id: 5,
            name: '5',
            img: photo5
        },

        {
            id: 6,
            name: '6',
            img: photo6
        }
    ]

    const items = arr.map(i => <Item key={i.id} name={i.name} img={i.img}/>)


    return (
        <div className={styles.wrapper}>
            {items}
        </div>
    )
}

export const UsersPage = () => {
    return (
        // <Layout>
        <div>
            <UsersList/>
        </div>
        // </Layout>
    )
}