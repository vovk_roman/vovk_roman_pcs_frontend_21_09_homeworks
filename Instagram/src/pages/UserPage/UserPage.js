import React from 'react'
import styles from './UserPage.module.css'
import {Layout} from '../../components/Layout/Layout'
import {UsersPage} from "../UsersPage";
import logo from '../../assets/images/logo.png'
import group from '../../assets/images/group.png'

export const UserPage = () => {
    return (
        <Layout>
            <div className={styles.wrapper}>
                <div className={styles.header}>
                    <div className={styles.logo}>
                        <img alt="Фото профиля" src={logo}/>
                    </div>

                    <section className={styles.description}>

                        <div className={styles.description__name}>
                            <h3>leomessi</h3>
                        </div>

                        <div className={styles.description__subscribe}>
                            <div className={styles.description__public}><b>812</b> публикаций</div>
                            <div className={styles.description__public}><b>289млн</b> подписчиков</div>
                            <div className={styles.description__public}><b>282</b> подписок</div>
                        </div>

                        <div className={styles.description__down}>
                            <h1 className="Yk1V7">Leo Messi</h1>
                            <br/><p>Bienvenidos a la cuenta oficial de Instagram de Leo Messi / Welcome to the official
                            Leo Messi Instagram account</p>

                        </div>
                        <div className={styles.description__features}>
                            <a href="/">messi.com</a>
                        </div>
                    </section>
                </div>
                <div className={styles.groups}>

                    <img src={group} alt="groups"/>
                    <hr/>
                </div>

                <div>
                </div>

                <UsersPage/>
            </div>
        </Layout>
    )
}
