import React, {useState} from "react";
import styles from './AuthForm.module.css'
import {setAuth, setRegistration, getRegistration} from "../../../../services/authStorageService";
import {observer} from "mobx-react-lite";
import AuthState from "../../../../store/AuthState";
import instagram from "../../../../assets/images/instagram.png";
import facebook from "../../../../assets/images/facebook.png";
import {Link} from "react-router-dom";

export const AuthForm = observer(() => {
    const [form, setForm] = useState({login: '', password: ''})
    const [check, setCheck] = useState(false)
    const [error, setError] = useState([])

    const changeForm = (e) => {
        if (e.currentTarget.name === 'login') {
            setError(error.filter(i => i !== 'login'))
        }
        if (e.currentTarget.name === 'password') {
            setError(error.filter(i => i !== 'password'))
        }

        setForm({...form, [e.currentTarget.name]: e.currentTarget.value})
    }

    const changeCheckBox = () => {
        setError(error.filter(i => i !== 'check'))
        setCheck(!check)
    }

    const loginHandler = () => {

        setError([])

        let arrError = []

        const validLogin = form.login.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if (validLogin === null) {
            arrError.push('login')
        }


        const validPassword = form.password.length < 4
        if (validPassword === true) {
            arrError.push('password')
        }
        let validCheckBox;
        validCheckBox = check;
        if (!validCheckBox) {
            arrError.push('check')
        }

        setError(arrError)
        if (arrError.length === 0) {
            const users = JSON.parse(getRegistration())
            if (users !== null) {
                const candidate = users.filter(i => i.login === form.login)
                if (candidate.length !== 0) {
                    if (candidate[0].password === form.password) {
                        setAuth()
                        return AuthState.loginHandler()
                    }
                    return alert('Пароль не правильный')
                }
                return alert('Пользоваткель с таким емэйл не найден')
            }
        }
    }

    const registrationHandler = () => {
        setRegistration(form.login, form.password)
    }

    return (
        <div className={styles.wrapper}>
            <div className={styles.form}>
                {/*<h1 className={styles.title}>Instagram</h1>*/}
                <img className={styles.instagram} src={instagram} alt={'instagram'}/>

                <input className={styles.input}
                       style={error.length !== 0 && error.filter(i => i === 'login').length !== 0 ? {border: '1px solid red'} : {}}
                       type='text' placeholder='Телефон, имя пользователя или эл. адрес' name='login' value={form.login}
                       onChange={changeForm}/>
                {error.length !== 0 && error.filter(i => i === 'login').length !== 0 &&
                <span>Email введен не верно</span>}
                <input className={styles.input}
                       style={error.length !== 0 && error.filter(i => i === 'password').length !== 0 ? {border: '1px solid red'} : {}}
                       type='text' placeholder='Пароль' name='password' value={form.password} onChange={changeForm}/>
                {error.length !== 0 && error.filter(i => i === 'password').length !== 0 &&
                <span>Пароль введен не верно</span>}
                <div className={styles.checkbox}>
                    <input className={styles.checkbox__check} type='checkbox' checked={check}
                           onChange={changeCheckBox}/>
                    <span>Я принимаю условия политики обработки персональных данных</span>
                </div>
                {error.length !== 0 && error.filter(i => i === 'check').length !== 0 &&
                <span>Нужно согласиться с условиями</span>}
                <button className={styles.button} onClick={loginHandler}>Войти</button>
                {/*<button className={styles.button} onClick={registrationHandler}>Регистрация</button>*/}

                <div className={styles.hr}>
                    <span/>
                    <p>или</p>
                    <span/>
                </div>

                <div className={styles.noData}>
                    <div>
                        <a className={styles.link} href={'https://facebook.com'} target={'_blank'}  rel={'noopener noreferrer'}>
                            <img className={styles.facebook} src={facebook} alt={'facebook'}/>
                            Войти через Facebook
                        </a>
                    </div>

                    <a className={styles.link} href={'https://www.instagram.com/accounts/password/reset/'}>
                        <div className={styles.forget}>
                            Забыли пароль?
                        </div>
                    </a>
                </div>
            </div>
            <div className={styles.register}>
                <p>У вас ещё нет аккаунта?
                    <Link to={''} onClick={registrationHandler}>Зарегистрироваться</Link>
                </p>
            </div>
        </div>
    )
})
