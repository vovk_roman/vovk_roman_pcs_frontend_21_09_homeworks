import React from "react";
import styles from './InstallApp.module.css'

export const InstallApp = () => {
    return (
        <div className={styles.wrapper}>
            <p>Установите приложение.</p>
            <div className={styles.buttons}>

                <a href={'https://itunes.apple.com/app/instagram/id389801252?pt=428156&ct=igweb.loginPage.badge&mt=8&vt=lo'} target={'_blank'} rel={"noopener noreferrer"}>
                    <img
                        src='https://www.instagram.com/static/images/appstore-install-badges/badge_ios_russian-ru.png/bfba6d0fd6bd.png'
                        alt={'iOS'}/>
                </a>

                <a href={'https://play.google.com/store/apps/details?id=com.instagram.android&referrer=utm_source%3Dinstagramweb%26utm_campaign%3DloginPage%26ig_mid%3D594A3097-A633-4783-812B-73A15E9DE98C%26utm_content%3Dlo%26utm_medium%3Dbadge'}  target={'_blank'} rel={"noopener noreferrer"}>
                    <img
                        src='https://www.instagram.com/static/images/appstore-install-badges/badge_android_russian-ru.png/4c70948c09f3.png'
                        alt={'Android'}/>
                </a>

            </div>
        </div>
    )
}