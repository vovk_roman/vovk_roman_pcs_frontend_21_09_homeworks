import React from "react";
import styles from './MainPage.module.css';
import {AuthForm} from "./components/AuthForm/AuthForm";
import {InstallApp} from "./components/InstallApp/InstallApp";
import {Footer} from "../../components/Footer/Footer";
import phones from '../../assets/images/phones.png';
import slide from '../../assets/images/slide.jpg';

export const MainPage = () => {
    return (
        <div className='container'>
            <div className={`${styles.wrapper}`}>
                <div>
                    <img className={styles.phone} src={phones} alt={'Phone'}/>
                    <div className={styles.slide_div} >
                        <img className={styles.slide} src={slide} alt={'Slide'}/>
                    </div>
                </div>


                <div>
                    <AuthForm/>
                    <InstallApp/>
                </div>
            </div>
            <Footer/>
        </div>
    )
}
