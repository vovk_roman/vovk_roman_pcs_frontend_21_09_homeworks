'use strict';

// Promise - Объекты которые используются для синхронных операций
const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve('ура!');
        // reject();
    }, 5000);
});
promise.then((result) => {
    console.log('Промис завершился успешно!', result);
}).catch((e) => {
    console.log('Пропис завершился неудачно :(');
}).finally(() => {
    console.log('Промис завершился');
});