'use strict';

// https://reqres.in/api/users

// fetch('https://reqres.in/api/users')
//     .then((response) => {
//         return response.json();
//     })
//     .then((body) => console.log(body));


// fetch('https://reqres.in/api/users')
//     .then((response) => {
//         return response.json();
//     })
//     .then((body) => {
//         body?.data.forEach((item) => {
//             console.log(item?.email);
//         })
//     });


// fetch('https://reqres.in/api/users?page=2')
//     .then((response) => {
//         return response.json();
//     })
//     .then((body) => {
//         body?.data.forEach((item) => {
//             console.log(item?.email);
//         })
//     });


fetch('https://reqres.in/api/users?per_page=12')
    .then((response) => {
        return response.json();
    })
    .then((body) => {
        body?.data.forEach((item) => {
            console.log(item?.email);
        })
    });