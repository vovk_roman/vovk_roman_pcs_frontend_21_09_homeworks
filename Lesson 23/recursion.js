'use strict';
// рекурсия
// function recursion() {
//     recursion();
// }
// recursion();


// function multipy(a, b) {
//     return a * b;
// }
//
// function  printSguare(x) {
//     const s = multiply(x, x);
//     console.log(s);
// }
//
// printSguare(5);


// let i = 0;
// function foo() {
//     i++;
//     foo();
// }
// try {
//     foo();
// } catch (e) {
//     console.log(i);
// }


function factorial(n) {
    if (n === 1) {
        return 1;
    }
    return n * factorial(n - 1);
}
const result = factorial(3);
console.log(result);