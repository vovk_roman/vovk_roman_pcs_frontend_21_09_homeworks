'use strict';

const EmailLabel = document.querySelector('.form__email-label');
const EmailInput = document.querySelector('.form__email');
const EmailEmpty = document.querySelector('.form__email-empty');
const EmailInvalid = document.querySelector('.form__email-invalid');
const EmailStar = document.querySelector('.form__email-star');

const PasswordLabel = document.querySelector('.form__password-label');
const PasswordInput = document.querySelector('.form__password');
const PasswordEmpty = document.querySelector('.form__password-empty');
const PasswordInvalid = document.querySelector('.form__password-invalid');
const PasswordStar = document.querySelector('.form__password-star');

const CheckboxInput = document.querySelector('.form__checkbox');
const CheckboxMark = document.querySelector('.form__checkbox-mark');
const CheckboxEmpty = document.querySelector('.form__checkbox-empty');
const CheckboxStar = document.querySelector('.form__checkbox-star');
const button = document.querySelector('.form__button');


button.addEventListener('click', (event) => {
    event.preventDefault(); // отменяет стандартное поведение кнопки

    // Email

    // функция проверки на валидность email
    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    // если в email ничего не заполнено
    if (EmailInput.value === '') {
        EmailInput.style.border = '1px solid #CB2424';
        EmailEmpty.style.display = 'block';
        EmailInvalid.style.display = 'none';
        EmailStar.style.color = '#CB2424';
        EmailLabel.style.color = '#CB2424';
    } else {

        if (validateEmail(EmailInput.value)) {
            EmailInput.style.border = '1px solid #787878';
            EmailEmpty.style.display = 'none';
            EmailInvalid.style.display = 'none';
            EmailStar.style.color = '#787878';
            EmailLabel.style.color = '#787878';

            // console.log('email: ', EmailInput.value);
        } else {
            //email невалидный
            EmailInput.style.border = '1px solid #CB2424';
            EmailEmpty.style.display = 'none';
            EmailInvalid.style.display = 'block';
            EmailLabel.style.color = '#CB2424';
            EmailStar.style.color = '#CB2424';
        }
    }

    // Password

    // если в password ничего не заполнено
    if (PasswordInput.value === '') {
        PasswordInput.style.border = '1px solid #CB2424';
        PasswordEmpty.style.display = 'block';
        PasswordInvalid.style.display = 'none';
        PasswordStar.style.color = '#CB2424';
        PasswordLabel.style.color = '#CB2424';

    } else if (PasswordInput.value.length > 7) {
        // Пароль содержит больше 7 символов
        PasswordInput.style.border = '1px solid #787878';
        PasswordEmpty.style.display = 'none';
        PasswordInvalid.style.display = 'none';
        PasswordStar.style.color = '#787878';
        PasswordLabel.style.color = '#787878';

        // console.log('password: ', PasswordInput.value);
    } else {
        // Пароль содержит больше 7 символов
        PasswordInput.style.border = '1px solid #CB2424';
        PasswordEmpty.style.display = 'none';
        PasswordInvalid.style.display = 'block';
        PasswordLabel.style.color = '#CB2424';
        PasswordStar.style.color = '#CB2424';
    }

    // Checkbox
    if (!(CheckboxInput.checked)) {
        CheckboxMark.style.border = '1px solid #CB2424';
        CheckboxEmpty.style.display = 'block';
        CheckboxStar.style.color = '#CB2424';
    }
    button.onclick = function () {
        if (CheckboxInput.checked) {
            CheckboxMark.style.border = '1px solid #787878';
            CheckboxEmpty.style.display = 'none';
            CheckboxStar.style.color = '#787878';

            // console.log('email: ', EmailInput.value);
            // console.log('password: ', PasswordInput.value);
        } else {
            CheckboxMark.style.border = '1px solid #CB2424';
            CheckboxEmpty.style.display = 'block';
            CheckboxStar.style.color = '#CB2424';
        }
    }

    // Если данные были введены верно, то при нажатии на "Регистрация" в консоль должен выводиться объект с данными
    if (validateEmail(EmailInput.value) && PasswordInput.value.length > 7 && CheckboxInput.checked) {
        const data = {
            email: EmailInput.value,
            password: PasswordInput.value
        }
        console.log(data);
    }
});

