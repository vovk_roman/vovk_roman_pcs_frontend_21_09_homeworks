'use strict';

setTimeout(() => {
    console.log('--------------------------------------------');
    console.log('1. Получить данные всех пользователей из https://reqres.in/api/users');
    console.log('--------------------------------------------');
    fetch('https://reqres.in/api/users?per_page=12')
        .then(response => response.json())
        .then((data) => {
            console.log(data);
        });
}, 1000);


setTimeout(() => {
    console.log('--------------------------------------------');
    console.log('2. Вывести в консоль фамилии всех пользователей в цикле');
    console.log('--------------------------------------------');
    fetch('https://reqres.in/api/users?per_page=12')
        .then((response) => {
            return response.json();
        })
        .then((body) => {
            body?.data.forEach((item) => {
                console.log(item?.last_name);
            })
        });
}, 1500);


setTimeout(() => {
    console.log('--------------------------------------------');
    console.log('3. Вывести все данные всех пользователей, фамилия которых начинается на F');
    console.log('--------------------------------------------');
    fetch('https://reqres.in/api/users?per_page=12')
        .then((response) => {
            return response.json();
        })
        .then((body) => {
            body?.data.forEach((item) => {
                if (item?.last_name[0] === 'F') {
                    console.log(item);
                }
            })
        });
}, 2000);


setTimeout(() => {
    console.log('--------------------------------------------');
    console.log('4. Вывести следующее предложение: ' +
        'Наша база содержит данные следующих пользователей:  ' +
        'и далее в этой же строке через запятую имена и фамилии всех пользователей. ' +
        'Использовать метод reduce');
    console.log('--------------------------------------------');

    fetch('https://reqres.in/api/users?per_page=12')
        .then(response => response.json())
        .then((body) => {
            const result = body.data.reduce((accumulator, item) => {
                accumulator = accumulator + (item.first_name + ' ' + item.last_name + ', ');
                return accumulator;
            }, '');
            console.log('Наша база содержит данные следующих пользователей: ', result);
        });
}, 2500);


setTimeout(() => {
    console.log('--------------------------------------------');
    console.log('5. Вывести названия всех ключей в объекте пользователя.');
    console.log('--------------------------------------------');

    fetch('https://reqres.in/api/users?per_page=12')
        .then((response) => {
            return response.json();
        })
        .then((body) => {
            body.data.forEach((item) => {
                console.log(item);
                Object.entries(item).forEach(([key, value]) => {
                    console.log('Ключ: ', key, 'Значение: ', value);
                })
            })
        });
}, 3000);