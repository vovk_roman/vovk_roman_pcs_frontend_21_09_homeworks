// Объявляем класса объекта
// class UserPage {
//
// }

/*

// Экспорт класса оъекта
export default class UserPage {
    //задаём свойства объекта через ;
    firstName;
    lastName;

    //напишем функцию, которая будет превращать объект в строчку
    toString() {
        return `${this.firstName} ${this.lastName}`;
    }
}
*/


// через this обращаемся к текущим свойствам текущего объекта

/*
// Конструктор задаёт какие-то начальные свойства, когда мы вызываем new UserPage()
export default class UserPage {
    //задаём свойства объекта через ;
    firstName;
    lastName;

    constructor(firstName = '', lastName = '') {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    toString() {
        return `${this.firstName} ${this.lastName}`;
    }
}
*/



// Приватные поля и приватные методы начинаются с нижнего подчеркивания
export default class User {
    _firstName;
    _lastName;

    //статичные методы
    // static getSomeInfo() {
    //     return 'This is user object';
    // }

    static isUser(obj) {
        return obj instanceof User;
    }

    constructor(firstName = '', lastName = '') {
        this._firstName = firstName;
        this._lastName = lastName;
    }

    set firstName(value) {
        this._firstName = value;
    }

    get firstName() {
        return `Имя: ${this._firstName}`;
    }

    set lastName(value) {
        this._lastNameName = value;
    }

    get lastName() {
        return this._lastName;
    }

    toString() {
        return `${this._firstName} ${this._lastName}`;
    }
}





