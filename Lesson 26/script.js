/*
'use strict';
let user = {
    name: 'Roman',
    age: 42,

    // три вида объявления функции можно использовать:
    // writeNameAndAge: function () {}
    // writeNameAndAge: () => {}
    // writeNameAndAge() {}

    writeNameAndAge() {
        console.log(`${this.name} ${this.age}`);
        // console.log(`${user.name} ${user.age}`);
    }
}

const user2 = user;
user = null;
user2.writeNameAndAge();
*/
/*

//Импорт класса объекта
import UserPage from './user.js';

const roman = new UserPage();
//зададим свойства для экземпляра класса:
roman.firstName = 'Roman';
roman.lastName = 'Vovk';
// console.log(roman); // выведет экземпляр класса
console.log(roman.toString()); // выведет экземпляр класса с преобразованием к строке

// создадим новый объект
const boris = new UserPage('Boris', 'Ivanov');
console.log(boris.toString());
*/

// Тоже что и выше только, через get и set
import User from './user.js';
import Admin from './admin.js';

const roman = new User();
roman.firstName = 'Roman';
roman.lastName = 'Vovk';

console.log(roman.toString());
console.log(roman.firstName);

const boris = new User('Boris', 'Ivanov');
console.log(boris.toString());

const permissions = [
    'WRITE_DB',
    'WRITE_USER',
    'EDIT_RIGHTS'
]
const admin = new Admin('FirstName', 'LastName', permissions, '1@1.ru');
console.log(admin.toString());

//вызов статичной функции
// UserPage.getSomeInfo();

// console.log(UserPage.isUser(admin));
console.log(Admin.isAdmin(boris));

