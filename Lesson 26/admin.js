import User from './user.js';

export default class Admin extends  User {
    _permissons;
    _email;

    static isAdmin(obj) {
        return obj instanceof Admin;
    }


    constructor(firstName = '', lastName = '', permissions = [], email = '') {
        super(firstName, lastName);

        this._permissons = permissions;
        this._email = email;
    }

    get permissons() {
        return this._permissons;
    }

    set permissons(value) {
        this._permissons = value;
    }

    get email() {
        return this._email;
    }

    set email(value) {
        this._email = value;
    }

    toString() {
        return `${super.toString()} [${this.permissons.join(',')}] ${this._email}`;
    }
}