/*
    + сложение
    - вычитание
    * умножение
    / деление
    % остаток от деления
    ** возведение в степень
    = присваивание
    *= сокращенная арифметика с присваиванием (домножение)
    += сокращенная арифметика с присваиванием (прибавление)
    ++ инкремент
    -- декремент
 */

const multiply = 5 ** 4;
console.log(multiply);

const restDivision = 28 % 5;
console.log(restDivision);

const multiply2 = 5 + 5 * 5;
console.log(multiply2);

/*
const str1 = 'Привет, ';
const str2 = 'мир!';
console.log(str1 + str2);
*/

const str1 = 'Привет, ';
const str2 = 'мир!';
const num = 5;
const str3 = str1 + str2 + num;
console.log(str3);

const stringNumber = '5';
console.log(+stringNumber + 25);

let n = 2;
n = n + 5;
n +=5;
n = n * 5;
n *= 5;
n *= 5 + 5;
console.log(n);

// boxing - оборачивание под капотом в объект
console.log('привет'.toUpperCase());

// unboxing - разворачивание из объекта в примитивный тип
console.log(Number(5));

let number = 0;
console.log('Инкремент возвращаемое', --number);
console.log('Инкремент', number);