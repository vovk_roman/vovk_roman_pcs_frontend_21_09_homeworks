import './App.css';
import {Routes, Route, Outlet} from "react-router-dom";
import {Link} from "react-router-dom";
import Fetch from "./components/Fetch/Fetch";
import UserPage from "./components/UserPage/UserPage";

function App() {
    return (
        <Routes>
            <Route path={'/'} element={<Layout/>}>
                <Route path={'users'} element={<UsersLayout/>}>
                    <Route index element={<Fetch/>}/>
                    <Route path={':userId'} element={<UserPage/>}/>
                </Route>
            </Route>
        </Routes>
    );
}

function UsersLayout() {
    return (
        <Outlet/>
    )
}

function Layout() {
    return (
        <div>
            <Link to={'users'} className={'link'}>
                <h2>Список пользователей</h2>
            </Link>
            <Outlet/>
        </div>
    )
}

export default App;
