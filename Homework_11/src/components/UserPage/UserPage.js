import React from "react";
import {useParams} from "react-router-dom";

export default function UserPage(props) {
    //деструктуризируем пропсы
    const {id, email, firstName, lastName, avatar} = props;
    const {userId} = useParams();
    return (
        <div className={'card'}>
            <div className="card__image">
                <img src={avatar} alt={''}/>
            </div>
            <h2 className={'card__title'}>{id} {userId} {firstName} {lastName}</h2>
            <div className={'card__subtitle'}>
                <a href={`mailto:${email}`}>{email}</a>
            </div>
        </div>
    );


}


UserPage.defaultProps = {
    id: '',
    email: '',
    firstName: '',
    lastName: '',
    avatar: ''
}