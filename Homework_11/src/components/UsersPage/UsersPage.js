import React from "react";
import UserPage from "../UserPage/UserPage";
import {Link} from "react-router-dom";

export default class UsersPage extends React.Component {
    static defaultProps = {
        userList: []
    }
    id;

    render() {
        return (
            <div className={'card-list'}>
                {
                    this.props.userList.map((user) => {
                        return (
                            <div className={'card-list__item'} key={user.id}>
                                <Link to={`${user.id}`} className={'link'}>
                                    <UserPage
                                        id={user.id}
                                        email={user.email}
                                        firstName={user['first_name']}
                                        lastName={user['last_name']}
                                        avatar={user['avatar']}/>
                                </Link>
                            </div>
                        )
                    })
                }
            </div>
        );
    }
}
