import React from 'react';
import UsersPage from "../UsersPage/UsersPage";

export default class Fetch extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: []
        }
    }

    componentDidMount() {
        fetch('https://reqres.in/api/users?per_page=12')
            .then((response) => response.json())
            .then((result) => {
                this.setState({users: result.data});
            })
    }

    renderUsers() {
        if (this.state.users.length) {
            return <UsersPage userList={this.state.users}/>
        }
        return <div>Загрузка...</div>
    }

    render() {
        return (
            <div className="App">
                {this.renderUsers()}
            </div>
        );
    }
}
