import './Button.css';
import {useState} from "react";
import classNames from "classnames";

function Button() {
    const [classNum, setClassNum] = useState(0);

    function changeClassNum() {
        if (classNum < 2) {
            setClassNum(classNum + 1);

            return;
        }

        setClassNum(0);
    }


    return (
        <button className={classNames('button',
            {
                'button_lime': classNum === 0,
                'button_orange': classNum === 1,
                'button_blue': classNum === 2
            })} onClick={changeClassNum}>Click me!</button>
    )
}

export default Button;