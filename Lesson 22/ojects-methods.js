'use strict';

const person = {
    name: 'Андрей',
    lastName: 'Гулин',
    age: 25,
    height: 183,
    weight: 70
};

//const personCopy = {...person};

const personBase = {
    user: true,
    name: 'Борис'
};

// Копировать объект
const personCopy = Object.assign({}, person);
console.log(personCopy);


//Object.values позволяет пробежаться по массиву, как по циклу.
// Он возвращает массив.
// И далее используем forEach чтобы пробежаться по этому массиву
Object.values(person).forEach((value) => {
    console.log(value);
});

//Вывести ключ  и значение
Object.entries(person).forEach(([key, value]) => {
    console.log('Ключ', key, 'Значение', value);
})
// Тоже самое что и выше
// Object.entries(person).forEach((value) => {
//     console.log('Ключ', value[0], 'Значение', value[1]);
// })