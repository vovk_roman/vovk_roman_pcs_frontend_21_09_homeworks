'use strict';

// создаем map
const map = new Map();
// устанавливаем в него значение
// map.set('a', '1');
// вывести его значение
// console.log(map.get('a'));
// вывести map полностью
// console.log(map);

// как пробежаться по map
map.set('a', 1);
map.set('b', 2);
map.set('c', 3);
map.set('d', 4);
// for..of
for (let [key, value] of map) {
    console.log(key, value);
}

for (let entry of map) {
    console.log(entry[0], entry[1]);
}
//вывести количество элементов в map
console.log(map.size);
