'use strict';

// set - набор уникальных значений

//создание set
const set = new Set();
// добавление значений в set
set.add('яблоко');
set.add('яблоко1');
set.add('яблоко2');
set.add('яблоко3');
console.log(set);

// задача
const arr = [1, 1, 3, 5,  5, 5, 6, 7, 8, 8];
// надо удалить повторяющиеся
console.log(new Set(arr));

// пробежаться по set  и вывести в консоль его значения
for (let entry of set) {
    console.log(entry);
}

const setObjects = new Set();
setObjects.add({a: 1});
setObjects.add({a: 1});
console.log(setObjects);
