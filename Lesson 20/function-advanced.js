'use strict';
// hoisting
// console.log(x);
// var x = 'qwe';
//
//
// console.log(y);
// let y = 'qwe';

sayHi();

// function declaration - можем использовать функцию до или после её объявления
function sayHi() {
    console.log('Hello!');
}

// function expression - Не можем использовать функцию до того, как она объявлена в константе
const sayHi2 = function () {
    console.log('Hello 2!');
}

const sayHi3 = () => {
    console.log('Hello 3!');
}

// IIFE - Immediately Invoked Function Expression
(function (name) {
    console.log('IIFE', name);
})('Andrew');

sayHi2();
sayHi3();

let name = 'Андрей';
function closure() {
    // let name = 'Andrew';
    console.log(name);
}
closure();

function createCounter () {
    let counter = 0;
    function add () {
        return ++counter;
    }

    return add;
}

const counter = createCounter();
console.log(counter());
console.log(counter());

let func;
// let i;
for (let i = 0; i < 10; i++) {
    if (i === 5) {
        func = function () {
            console.log(i);
        }
    }
}

// console.log('Счётчик', i);
func();



let variable = 'asd';

if (true) {
    let variable2 = 'asd';
    var variable3 = 'var3';
}
console.log(variable);
console.log(variable3);
console.log(variable2);
